//Language features
// Times

function times(arr: Array<number>, num: number = 2): number[] {
  let newArr: number[] = [];
  for (let j = 0; j < num; j++) {
    for (let i = 0; i < arr.length; i++) {
      newArr.push(arr[i]);
    }
  }
  console.log(newArr);
  return newArr;
}
times([1, 2, 3], 3);

// logger;
function logger(
  arr: string[],
  obj: { serviceName: string; serviceId: number } = {
    serviceName: "global",
    serviceId: 1,
  }
): any {
  const result: { [key: string]: string } = {};
  for (let i = 0; i < arr.length; i++) {
    result[`${obj.serviceId}-${i}`] = `[${obj.serviceName}] ${arr[i]}`;
  }
  return result;
}
console.log(
  logger(["Wrong email", "Wrong password", "Success login"], {
    serviceName: "auth_service",
    serviceId: 3,
  })
);

// Array NTH Element
function everyNth(arr: Array<number>, nthNum: number = 1): number[] {
  const filteredArray = arr.filter((item) => item % nthNum === 0);
  if (nthNum === 1) {
    return arr;
  } else {
    return filteredArray;
  }
}

console.log(everyNth([1, 2, 3, 4, 5, 6], 3));

// Types........
// Days to New Year
function getDaysToNewYear(date: Date | string) {
  const newYear = new Date(2021, 12, 31);
  const newYearDay = newYear.getTime() / 1000 / 60 / 60 / 24;
  let dateDay;
  if (date instanceof Date) {
    dateDay = date.getTime() / 1000 / 60 / 60 / 24;
  } else if (typeof date === "string") {
    const [day, month, year] = date.split(".");
    dateDay =
      new Date(parseInt(year), parseInt(month), parseInt(day)).getTime() /
      1000 /
      60 /
      60 /
      24;
  } else {
    return new Error("Invalid date format");
  }

  let returnedDay = newYearDay - dateDay;

  return returnedDay !== 0 ? returnedDay : 1;
}
const today = new Date();
const daysUntilNewYear = getDaysToNewYear(new Date(2021, 12, 31));
console.log(daysUntilNewYear);
// Last to first
function lastToFirst(str: string): void {
  let reversed: string = str.split("").reverse().join("");
  console.log(reversed);
}

lastToFirst("loop");

// Group organization users
const users = [
  {
    name: "Bill",
    login: "bill01",
    surname: "Jobs",
    type: "EMPLOYEE",
    address: { officeId: 123, placeId: 1222 },
  },
  {
    name: "Fill",
    login: "fill007",
    surname: "Filler",
    type: "CONTRACTOR",
    contractorCompanyName: "Microsoft",
  },
  {
    name: "Alex",
    login: "alex777",
    type: "EMPLOYEE",
    address: { officeId: 222, placeId: 333 },
  },
  {
    name: "John",
    login: "coolJohn",
    type: "CONTRACTOR",
    contractorCompanyName: "Apple",
  },
];

interface IUser {
  name: string;
  login: string;
  surname?: string;
  type: string;
  address?: { officeId: number; placeId: number };
  contractorCompanyName?: string;
}

interface IResult {
  employees: IUser[];
  contractors: IUser[];
}

function groupUsers(arr: IUser[]): IResult {
  const result: IResult = {
    employees: [],
    contractors: [],
  };
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].type === "EMPLOYEE") {
      result.employees.push(arr[i]);
    } else {
      result.contractors.push(arr[i]);
    }
  }
  return result;
}

// console.log(groupUsers(users));

// Classes
// Vehicle

class Vehicle {
  constructor(vendor: string, model: string) {
    this.vendor = vendor;
    this.model = model;
    this.capacity = 40;
    this.fuelLevel = 40;
    this.fuelConsumption = 1;
    this.startConsumption = 3;
    this.interval = 0;
  }
  vendor: string;
  model: string;
  capacity: number;
  fuelLevel: number;
  fuelConsumption: number;
  startConsumption: number;
  interval: number;

  start() {
    this.fuelLevel = this.fuelLevel - 3;
    this.interval = setInterval(() => {
      this.fuelLevel -= 1;
    }, 1000);
  }

  refuel() {
    this.fuelLevel = this.capacity;
  }
  turnOff() {
    clearInterval(this.interval);
    // console.log(this.fuelLevel);
  }
}

const car1 = new Vehicle("Toyota", "Corolla");

car1.start();

setTimeout(() => {
  car1.turnOff();
}, 4100);

// Counter


interface IGetInstance {
  getState:() => number;
  increase:() => void;
  decrease:() => void;
}

class Counter {
  static counter = 0;
  static destroy() {
    this.counter = 0;
  }

  static getInstance(): IGetInstance {
    const getState = () => this.counter;
    const increase = () => this.counter++;
    const decrease = () => this.counter--;
    return {
      increase,
      decrease,
      getState,
    };
  }
}

const instance1 = Counter.getInstance();
instance1.increase();
console.log(instance1.getState(),'instance1'); // 1;
const instance2 = Counter.getInstance();
console.log(instance2.getState(),'instance2'); // 1;
instance2.increase();
console.log(instance1.getState(),'instance1'); // 2;
console.log(instance2.getState(),'instance2'); // 2;
Counter.destroy();
console.log(Counter.getInstance().getState()); // 0
