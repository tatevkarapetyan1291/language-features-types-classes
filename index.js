//Language features
// Times
function times(arr, num) {
    if (num === void 0) { num = 2; }
    var newArr = [];
    for (var j = 0; j < num; j++) {
        for (var i = 0; i < arr.length; i++) {
            newArr.push(arr[i]);
        }
    }
    console.log(newArr);
    return newArr;
}
times([1, 2, 3], 3);
// logger;
function logger(arr, obj) {
    if (obj === void 0) { obj = {
        serviceName: "global",
        serviceId: 1
    }; }
    var result = {};
    for (var i = 0; i < arr.length; i++) {
        result["".concat(obj.serviceId, "-").concat(i)] = "[".concat(obj.serviceName, "] ").concat(arr[i]);
    }
    return result;
}
console.log(logger(["Wrong email", "Wrong password", "Success login"], {
    serviceName: "auth_service",
    serviceId: 3
}));
// Array NTH Element
function everyNth(arr, nthNum) {
    if (nthNum === void 0) { nthNum = 1; }
    var filteredArray = arr.filter(function (item) { return item % nthNum === 0; });
    if (nthNum === 1) {
        return arr;
    }
    else {
        return filteredArray;
    }
}
console.log(everyNth([1, 2, 3, 4, 5, 6], 3));
// Types........
// Days to New Year
function getDaysToNewYear(date) {
    var newYear = new Date(2021, 12, 31);
    var newYearDay = newYear.getTime() / 1000 / 60 / 60 / 24;
    var dateDay;
    if (date instanceof Date) {
        dateDay = date.getTime() / 1000 / 60 / 60 / 24;
    }
    else if (typeof date === "string") {
        var _a = date.split("."), day = _a[0], month = _a[1], year = _a[2];
        dateDay =
            new Date(parseInt(year), parseInt(month), parseInt(day)).getTime() /
                1000 /
                60 /
                60 /
                24;
    }
    else {
        return new Error("Invalid date format");
    }
    var returnedDay = newYearDay - dateDay;
    return returnedDay !== 0 ? returnedDay : 1;
}
var today = new Date();
var daysUntilNewYear = getDaysToNewYear(new Date(2021, 12, 31));
console.log(daysUntilNewYear);
// Last to first
function lastToFirst(str) {
    var reversed = str.split("").reverse().join("");
    console.log(reversed);
}
lastToFirst("loop");
// Group organization users
var users = [
    {
        name: "Bill",
        login: "bill01",
        surname: "Jobs",
        type: "EMPLOYEE",
        address: { officeId: 123, placeId: 1222 }
    },
    {
        name: "Fill",
        login: "fill007",
        surname: "Filler",
        type: "CONTRACTOR",
        contractorCompanyName: "Microsoft"
    },
    {
        name: "Alex",
        login: "alex777",
        type: "EMPLOYEE",
        address: { officeId: 222, placeId: 333 }
    },
    {
        name: "John",
        login: "coolJohn",
        type: "CONTRACTOR",
        contractorCompanyName: "Apple"
    },
];
function groupUsers(arr) {
    var result = {
        employees: [],
        contractors: []
    };
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].type === "EMPLOYEE") {
            result.employees.push(arr[i]);
        }
        else {
            result.contractors.push(arr[i]);
        }
    }
    return result;
}
// console.log(groupUsers(users));
// Classes
// Vehicle
var Vehicle = /** @class */ (function () {
    function Vehicle(vendor, model) {
        this.vendor = vendor;
        this.model = model;
        this.capacity = 40;
        this.fuelLevel = 40;
        this.fuelConsumption = 1;
        this.startConsumption = 3;
        this.interval = 0;
    }
    Vehicle.prototype.start = function () {
        var _this = this;
        this.fuelLevel = this.fuelLevel - 3;
        this.interval = setInterval(function () {
            _this.fuelLevel -= 1;
        }, 1000);
    };
    Vehicle.prototype.refuel = function () {
        this.fuelLevel = this.capacity;
    };
    Vehicle.prototype.turnOff = function () {
        clearInterval(this.interval);
        // console.log(this.fuelLevel);
    };
    return Vehicle;
}());
var car1 = new Vehicle("Toyota", "Corolla");
car1.start();
setTimeout(function () {
    car1.turnOff();
}, 4100);
var Counter = /** @class */ (function () {
    function Counter() {
    }
    Counter.destroy = function () {
        this.counter = 0;
    };
    Counter.getInstance = function () {
        var _this = this;
        var getState = function () { return _this.counter; };
        var increase = function () { return _this.counter++; };
        var decrease = function () { return _this.counter--; };
        return {
            increase: increase,
            decrease: decrease,
            getState: getState
        };
    };
    Counter.counter = 0;
    return Counter;
}());
var instance1 = Counter.getInstance();
instance1.increase();
console.log(instance1.getState(), 'instance1'); // 1;
var instance2 = Counter.getInstance();
console.log(instance2.getState(), 'instance2'); // 1;
instance2.increase();
console.log(instance1.getState(), 'instance1'); // 2;
console.log(instance2.getState(), 'instance2'); // 2;
Counter.destroy();
console.log(Counter.getInstance().getState()); // 0
